# Le projet Rzine [<img src="figure/Rzine.png" align="right" width="120"/>](http://rzine.fr/)

### De l'article de données à l'article de code, en passant par les méthodes

Séminaire de Méthodologie (MetSem) - RésIn, SciencePo Paris, Juin 2024

➡️ [support de présentation](https://metsem-rzine-communications-24179b2cc4cff52abc887fc720c90e98a96.gitpages.huma-num.fr/#/title-slide)
